
import { useAuthentication } from "#/hooks/useAuthentication";
import { useNotification } from "#/hooks/useNotification";
import { Box, Button, createStyles, Grid, makeStyles, Paper, TextField } from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";
import { isEmpty, throttle } from "lodash";
import React, { ChangeEvent, FocusEvent, FunctionComponent, useCallback, useEffect, useState } from "react";
import { FaGoogle } from "react-icons/fa";


type Credential = {
	email: string,
	password: string,
	signInMethods: string[],
	step: LoginStep;
};

type LoginStep = "initial" | "validating" | "newUser" | "existingUser";

const useStyles = makeStyles((theme) =>
	createStyles({
		root: {
			"& .MuiTextField-root": {
				margin: theme.spacing(2),
			},
			"& .MuiButton-root": {
				margin: theme.spacing(2),
			},
		},
	}),
);

const Login: FunctionComponent = () => {
	const cssStyles = useStyles();
	const [{ notificateDefault, notificateError }] = useNotification();
	const [{ loginWithEmail, loginWithGoogle, fetchSignInMethodsForEmail, createAccount }] = useAuthentication();
	const [credentials, setCredentials] = useState<Credential>({
		email: "",
		password: "",
		signInMethods: [],
		step: "initial"
	});
	const [authenticateButtonState, setAuthenticateButtonState] = useState({
		children: "Próximo",
		disabled: false,
	});

	const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
		throttle(() => {
			setCredentials((prev) => ({
				...prev,
				email: event.target.value
			}));
		}, 500)();
	};


	const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
		throttle(() => {
			setCredentials((prev) => ({
				...prev,
				password: event.target.value
			}));
		}, 500)();
	};


	const checkUser = useCallback(async () => {
		if (credentials.step === "validating") return;
		try {
			if (credentials.email) {

				setCredentials((prev) => ({ ...prev, step: "validating" }));

				const signInMethods = await fetchSignInMethodsForEmail(credentials.email);

				if (isEmpty(signInMethods)) {
					setCredentials((prev) => ({ ...prev, signInMethods, step: "newUser" }));
				}
				else if (signInMethods.includes("password"))
					setCredentials((prev) => ({ ...prev, signInMethods, step: "existingUser" }));
				else
					setCredentials((prev) => ({ ...prev, signInMethods, step: "existingUser" }));
			} else {

				setCredentials((prev) => ({ ...prev, step: "initial" }));

			}
		} catch (error) {
			console.error("check_user", error);
			setCredentials((prev) => ({ ...prev, step: "initial" }));
		} 
	}, [credentials.email, credentials.step, fetchSignInMethodsForEmail]);


	const handleEmailBlur = useCallback(async (event: FocusEvent<HTMLInputElement>) => {
		await checkUser();
	}, [checkUser]);


	const handleGoogleLogin = useCallback(async () => {
		try {
			const { user } = await loginWithGoogle();
			notificateDefault(`Logado como ${user?.email!}`);
		} catch (error) {
			console.error("google_login_error", error);
			notificateError("Ocorreu um erro ao efetuar login.");
		}
	}, [loginWithGoogle, notificateDefault, notificateError]);

	const handleSubmit = useCallback(async () => {
		switch (credentials.step) {
			case "newUser": {
				try {
					const { user } = await createAccount(credentials.email, credentials.password);
					notificateDefault(`Logado como ${user?.email!}`);
				} catch (error) {
					console.error("create_account:error", error);
					notificateError("Ocorreu um erro ao criar cadastro.");
				}
				break;
			}
			case "existingUser": {
				try {
					if (credentials.signInMethods.includes("password")) {
						const { user } = await loginWithEmail(credentials.email, credentials.password);
						notificateDefault(`Logado como ${user?.email!}`);
					} else if (credentials.signInMethods.includes("google.com")) {
						await handleGoogleLogin();
					}
				} catch (error) {
					console.error("login_account:error", error);
					notificateError("Ocorreu um erro ao efetuar login.");
				}
				break;
			}
			default: { break; }
		}
	}, [createAccount, credentials, handleGoogleLogin, loginWithEmail, notificateDefault, notificateError])


	useEffect(() => {

		switch (credentials.step) {
			case "initial": {
				setAuthenticateButtonState({
					children: "Próximo",
					disabled: false
				});
				break;
			}
			case "newUser": {
				setAuthenticateButtonState({
					children: "Cadastrar",
					disabled: false
				});
				break;
			}
			case "existingUser": {
				setAuthenticateButtonState({
					children: "Login",
					disabled: false
				});
				break;
			}
			case "validating": {
				setAuthenticateButtonState({
					children: "Aguarde",
					disabled: true
				});
				break;
			}
		}

	}, [credentials.step]);


	return (<>

		<Grid container justify="center" alignItems="center" alignContent="center" spacing={1}>

			<Grid item xs={10} sm={6} md={4}>

				<Box my={10}>

					<Paper elevation={5} className={cssStyles.root}>

						<Box display="flex" flexDirection="column" alignItems="center" p={2}>

							<AccountCircle style={{ fontSize: "100px" }} />

							<TextField
								label="Email"
								value={credentials.email}
								onChange={handleEmailChange}
								onBlur={handleEmailBlur}
								type="email"
								fullWidth
							/>

							{(credentials.step === "newUser" || credentials.step === "existingUser") &&
								<TextField
									label="Senha"
									value={credentials.password}
									type="password"
									onChange={handlePasswordChange}
									fullWidth
								/>
							}

							<Button
								children={authenticateButtonState.children}
								onClick={handleSubmit}
								color="primary"
								variant="contained"
								fullWidth
								disabled={authenticateButtonState.disabled}
							/>

							<Button
								children={"Entrar com Google"}
								onClick={handleGoogleLogin}
								startIcon={(<FaGoogle />)}
								color="primary"
								variant="outlined"
								fullWidth
							/>

						</Box>

					</Paper>

				</Box>

			</Grid>

		</Grid>

	</>);
};

export default Login;