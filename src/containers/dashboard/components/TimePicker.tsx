import { useNotification } from "#/hooks/useNotification";
import { Nullable } from "#/types";
import { isAfter, isValidDate, LOCAL_TIME_FORMAT, ParsableDate, parseDate } from "#/utils/date.utils";
import { Box } from "@material-ui/core";
import { KeyboardTimePicker } from "@material-ui/pickers";
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import React, { FunctionComponent, useCallback, useEffect, useState } from "react";

type Props = {
	label?: string;
	value?: Nullable<string>,
	readOnly?: boolean;
	minTime?: Nullable<string>;
	minTimeMessage?: string;
	onChange?: (date: MaterialUiPickersDate, value?: string | null | undefined) => void
}

const TimePicker: FunctionComponent<Props> = ({ label, value, readOnly, minTime, minTimeMessage, onChange }) => {

	const [{ notificateError }] = useNotification();
	const [internalValue, setInternalValue] = useState<Nullable<ParsableDate>>(value || null);
	const [inputValue, setInputValue] = useState<string>();

	useEffect(() => {
		setInternalValue(value || null);
		setInputValue(value || undefined);
	}, [value]);


	const validateMinTime = useCallback((date: Nullable<ParsableDate>, value?: Nullable<string>) => {
		if (!minTime) return;
		if (!isValidDate(date)) return;

		const minDate = parseDate(minTime, LOCAL_TIME_FORMAT);

		if (isAfter(minDate, date!)) {
			throw new Error("Time should be greater then minTime");
		}
	}, [minTime]);

	const handleChange = useCallback((date: MaterialUiPickersDate, value?: string | null) => {
		if (!onChange) return () => { };

		try {
			validateMinTime(date, value);
			return onChange(date, value);
		} catch (error) {
			notificateError(minTimeMessage);
			setInternalValue(null);
			setInputValue(undefined);
		}
	}, [minTimeMessage, notificateError, onChange, validateMinTime]);

	return (
		<Box style={{ boxSizing: 'content-box' }}>

			<KeyboardTimePicker
				label={label}
				placeholder="09:00"
				mask="__:__"
				value={internalValue}
				inputValue={inputValue}
				onChange={handleChange}
				disabled={readOnly}
				minDateMessage={minTimeMessage}
				ampm={false}
				variant="inline"
				invalidDateMessage={"Formato de hora inválido"}
			/>

		</Box>
	)
};

export default TimePicker;