import { useFilledTimesheet } from "#/hooks/useFilledTimesheet";
import { useSortBy } from "#/hooks/useSortBy";
import { dateFieldFormatter, durationFormatter, remainingHoursGetter, resolveCellCssClass, workingHoursGetter } from "#/utils/timesheet.utils";
import { Box, makeStyles } from "@material-ui/core";
import grey from '@material-ui/core/colors/grey';
import { DataGrid, GridColDef, GridRowParams } from "@material-ui/data-grid";
import React, { FunctionComponent, useCallback } from "react";
import { TimesheetEntry } from "./TimeEntryDialog";
import TimesheetToolbar from "./TimesheetToolbar";


type Props = {
	month: Date;
	entries: TimesheetEntry[];
	onMonthChange: (month: Date) => void;
	onRowSelect: (row: TimesheetEntry) => void;
};

const useStyles = makeStyles(theme => ({
	root: {
		"& .cell.--secondary": {
			backgroundColor: grey[200],
		},
		"& .cell.--success": {
			color: theme.palette.success.main,
		},
		"& .cell.--danger": {
			color: theme.palette.error.main,
		}
	},
}));


const columns = Object.freeze<GridColDef[]>([
	{
		field: "date",
		headerName: "Data",
		// width: 200,
		flex: 1,
		type: "string",
		valueFormatter: dateFieldFormatter,
		cellClassName: resolveCellCssClass,
	},
	{
		field: "startTime",
		headerName: "Entrada",
		type: "string",
		sortable: false,
		headerAlign: "center",
		align: "center",
		width: 130,
		cellClassName: resolveCellCssClass,
	},
	{
		field: "endTime",
		headerName: "Saída",
		type: "string",
		sortable: false,
		headerAlign: "center",
		align: "center",
		width: 130,
		cellClassName: resolveCellCssClass,
	},
	{
		field: "breakTime",
		headerName: "Pausa",
		type: "string",
		sortable: false,
		headerAlign: "center",
		align: "center",
		width: 130,
		cellClassName: resolveCellCssClass,
	},
	{
		field: "total",
		headerName: "Total",
		type: "string",
		sortable: false,
		headerAlign: "center",
		align: "center",
		width: 130,
		valueGetter: workingHoursGetter,
		valueFormatter: durationFormatter,
		cellClassName: resolveCellCssClass,
	},
	{
		field: "remainingHours",
		headerName: "Horas restantes",
		type: "string",
		sortable: false,
		headerAlign: "center",
		align: "center",
		width: 150,
		valueGetter: remainingHoursGetter,
		valueFormatter: durationFormatter,
		cellClassName: resolveCellCssClass,

	},
]);

const Timesheet: FunctionComponent<Props> = ({ entries, month, onMonthChange, onRowSelect }) => {

	const cssStyles = useStyles();
	const [rows] = useFilledTimesheet(entries, month);
	const [sortSettings] = useSortBy("date", "desc");


	const handleRowClick = useCallback((row: GridRowParams) => {
		onRowSelect(row.row as TimesheetEntry)
	}, [onRowSelect]);

	return (<>
		<Box minWidth="500px" maxWidth="1000px" height="812px" mx="auto" position="relative">

			<DataGrid
				className={cssStyles.root}
				columns={[...columns]}
				rows={[...rows]}
				onRowClick={handleRowClick}
				autoHeight={true}
				density={"compact"}
				pageSize={16}
				disableColumnMenu={true}
				disableColumnSelector={true}
				disableSelectionOnClick={true}
				sortModel={[sortSettings]}
				components={{
					Toolbar: TimesheetToolbar
				}}
				componentsProps={{
					toolbar: {
						rows: [...rows],
						month: month,
						onMonthChange: onMonthChange,
					}
				}}
			/>

		</Box>



	</>);
}

export default Timesheet;