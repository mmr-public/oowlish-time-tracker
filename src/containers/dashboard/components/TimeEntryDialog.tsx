import Loader from '#/components/Loader';
import { EMPLOYEE_ENTRIES_COLLECTION_PATH } from '#/constants';
import { Nullable } from '#/types';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Icon } from '@material-ui/core';
import { FirestoreDocument } from '@react-firebase/firestore';
import React, { FunctionComponent, SyntheticEvent, useCallback, useState } from 'react';
import TimeEntryForm, { FormState } from './TimeEntryForm';


export type TimesheetEntry = {
	date: string;
	id?: string;
	employeeId?: string;
	startTime?: Nullable<string>;
	endTime?: Nullable<string>;
	breakTime?: Nullable<string>;
	_createdAt?: Nullable<string>;
	_modifiedAt?: Nullable<string>;
};

export type Props = {
	entry: TimesheetEntry;
	show: boolean;
	onSubmit: (values: TimesheetEntry) => void;
	onClose?: () => void;
};

const TimeEntryDialog: FunctionComponent<Props> = ({ show, entry, onClose, onSubmit }) => {
	const [formState, setFormState] = useState<FormState>();

	const handleSave = useCallback((event: SyntheticEvent) => {
		if (formState?.isValid) {
			onSubmit({ ...formState.values, });
		}
	}, [formState, onSubmit]);

	return (<>
		<FirestoreDocument
			path={`${EMPLOYEE_ENTRIES_COLLECTION_PATH}/${entry.id}`}
			children={({ isLoading, path, value }) => (<>

				<Dialog open={show} onClose={onClose} onBackdropClick={onClose} maxWidth={"md"}>

					<DialogTitle id="form-dialog-title" children={"Registro de horário"} />

					<DialogContent>

						{isLoading && <Loader />}

						{!isLoading &&
							<>

								<DialogContentText children={"Preencha os horários de entrada, saída e duração de invervalo."} />

								<TimeEntryForm
									values={{
										...entry,
										...value,
									}}
									onChange={(formState) => {
										setFormState(() => ({ ...formState }));
									}}
								/>

							</>
						}

					</DialogContent>

					<DialogActions>

						<Button
							children={"Cancelar"}
							onClick={onClose}
							color="secondary"
						/>

						<Button
							children={"Salvar"}
							disabled={!formState?.isValid}
							color="primary"
							endIcon={<Icon>save</Icon>}
							onClick={handleSave}
						/>

					</DialogActions>

				</Dialog>

			</>)}
		/>
	</>)
};

export default TimeEntryDialog;



