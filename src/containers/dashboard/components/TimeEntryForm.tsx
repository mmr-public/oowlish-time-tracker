import { DATE_FNS_LOCALE_DATE_FORMAT, durationBetween, durationFromTime, formatDate, isValidTime, LOCALE_DATE_FORMAT } from '#/utils/date.utils';
import { Grid } from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { TimesheetEntry } from './TimeEntryDialog';
import TimePicker from './TimePicker';

export type FormState = {
	values: TimesheetEntry;
	isValid: boolean;
}

export type Props = {
	values: TimesheetEntry;
	onChange?: (formState: FormState) => void
}

const validateFields = ({ startTime, endTime, breakTime }: TimesheetEntry) => {
	const isValid = [startTime, endTime, breakTime]
		.reduce((isValid, current, i) => {
			if (!isValid) return false;
			return isValid && current ? isValidTime(current) : true
		}, true);
	if (!isValid)
		throw new Error("Invalid date format");
};

const validateBreakTime = ({ startTime, endTime, breakTime }: TimesheetEntry) => {

	if (startTime && endTime && breakTime) {
		const workedTime = durationBetween(startTime, endTime);
		const breakTimeDuration = durationFromTime(breakTime);

		if (breakTimeDuration.asMilliseconds() > workedTime.asMilliseconds())
			throw new Error("Break time is greater then worked hours");
	}
};


const TimeEntryForm: FunctionComponent<Props> = ({ values, onChange }) => {
	const [internalEntry, setInternalEntry] = useState(values);
	const handleChange = (fieldName: "startTime" | "endTime" | "breakTime") => (date: MaterialUiPickersDate, value?: string | null) => {
		setInternalEntry((prev) => ({
			...prev,
			[fieldName]: value,
		}));
	};

	useEffect(() => {
		try {
			validateFields(internalEntry);
			validateBreakTime(internalEntry);
			onChange && onChange({
				isValid: true,
				values: internalEntry,
			});
		} catch (error) {
			console.warn(error);
			onChange && onChange({
				isValid: false,
				values: internalEntry,
			});
		}

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [internalEntry]);



	return (<>

		<Grid container alignItems={"center"} alignContent={"center"} spacing={1}>

			<Grid item xs={3}>

				<KeyboardDatePicker
					label={"Data"}
					value={values.date}
					inputValue={formatDate(values.date, LOCALE_DATE_FORMAT)}
					onChange={() => { }}
					format={DATE_FNS_LOCALE_DATE_FORMAT}
					readOnly
					disabled
				/>

			</Grid>

			<Grid item xs={3}>

				<TimePicker
					label={"Horário de entrada"}
					value={internalEntry.startTime}
					onChange={handleChange("startTime")}
				/>

			</Grid>

			<Grid item xs={3}>

				<TimePicker
					label={"Horário de saída"}
					value={internalEntry.endTime}
					minTime={internalEntry.startTime}
					onChange={handleChange("endTime")}
					minTimeMessage={"O horário de saída deve ser maior que o horário de entrada."}
				/>

			</Grid>

			<Grid item xs={3}>

				<TimePicker
					label={"Tempo de intervalo"}
					value={internalEntry.breakTime}
					onChange={handleChange("breakTime")}
				/>

			</Grid>
			
		</Grid>

	</>);
};

export default TimeEntryForm;



