import { useMonthStatistics } from '#/hooks/useMonthAnalysis';
import { formatDuration, isValidDate } from '#/utils/date.utils';
import { Box, Grid, TextField } from '@material-ui/core';
import { GridBaseComponentProps } from '@material-ui/data-grid';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import React, { FunctionComponent, useEffect, useState } from "react";
import { TimesheetEntry } from './TimeEntryDialog';

type Props = {
	rows: TimesheetEntry[];
	month: MaterialUiPickersDate;
	onMonthChange: (month: MaterialUiPickersDate) => void
} & GridBaseComponentProps;

const TimesheetToolbar: FunctionComponent<Props> = ({ month, rows, onMonthChange }) => {
	const [activeMonth, setActiveMonth] = useState<MaterialUiPickersDate>(month);
	const [{ expectedHours, workedHours, balance, }] = useMonthStatistics(month as Date, rows);

	useEffect(() => {
		if (isValidDate(activeMonth))
			onMonthChange(activeMonth);
	}, [activeMonth, onMonthChange]);

	return (<>
		<Box my={2} px={2}>

			<Grid container alignContent="center" alignItems="center" justify="space-between" spacing={2}>

				<Grid item xs={12} sm={3} md={2} >

					<Box textAlign="center">

						<KeyboardDatePicker
							label="Mês de competência"
							value={activeMonth}
							format="MM/yyyy"
							views={["year", "month"]}
							maxDate={new Date()}
							onChange={setActiveMonth}
							size="small"
							style={{
								width: "200px",
								margin: "auto",
								boxSizing: "content-box",
							}}
						/>

					</Box>

				</Grid>

				<Grid item xs={12} sm={9} md={10}>

					<Grid container alignContent="center" alignItems="center" justify="flex-end" spacing={2}>

						<Grid item xs={12} sm={3} md={3}>

							<Box textAlign="center">

								<TextField
									label="Horas trabalhadas"
									value={formatDuration(workedHours.asMilliseconds())}
									disabled={true}
									size="small"
								/>

							</Box>

						</Grid>

						<Grid item xs={12} sm={3} md={3}>

							<Box textAlign="center">

								<TextField
									label="Horas do mês"
									value={formatDuration(expectedHours.asMilliseconds())}
									disabled={true}
									size="small"
								/>

							</Box>

						</Grid>

						<Grid item xs={12} sm={3} md={3}>

							<Box textAlign="center">

								<TextField
									label="Saldo do mês"
									value={formatDuration(balance.asMilliseconds())}
									disabled={true}
									size="small"
								/>

							</Box>

						</Grid>

					</Grid>

				</Grid>

			</Grid>

		</Box>
	</>);
}


export default TimesheetToolbar as FunctionComponent<GridBaseComponentProps>;