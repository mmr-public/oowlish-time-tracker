import Loader from '#/components/Loader';
import { EMPLOYEE_ENTRIES_COLLECTION_PATH } from '#/constants';
import withUser, { Props as WithUserProps } from '#/hocs/withUser';
import { useMonthLimits } from '#/hooks/useMonthLimits';
import { useNotification } from '#/hooks/useNotification';
import { useOrderBy } from '#/hooks/useOrderBy';
import { useTimesheetEntityBuilder } from '#/hooks/useTimesheetEntityBuilder';
import { formatDate, ISO_DATE_FORMAT } from '#/utils/date.utils';
import { Box, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { FirestoreCollection, FirestoreMutation } from '@react-firebase/firestore';
import { throttle } from 'lodash';
import React, { FunctionComponent, useCallback, useState } from "react";
import TimeEntryDialog, { TimesheetEntry } from './components/TimeEntryDialog';
import Timesheet from "./components/Timesheet";


const Dashboard: FunctionComponent<WithUserProps> = ({ user }) => {
	const [{ notificateDefault, notificateError }] = useNotification();
	const [month, setMonth] = useState<Date>(new Date());
	const [selectedRow, setSelectedRow] = useState<TimesheetEntry>();
	const [orderBy] = useOrderBy("date", "asc");
	const [{ startAt, endAt }] = useMonthLimits(month);
	const [buildEntity] = useTimesheetEntityBuilder(user.id);

	const handleAddEntry = useCallback(() => {
		const today = formatDate(new Date(), ISO_DATE_FORMAT);
		const entity = buildEntity({ date: today, });

		setSelectedRow(entity);
	}, [buildEntity]);

	const handleRowClick = useCallback((row) => {
		const entity = buildEntity({ ...row, });
		setSelectedRow(entity);
	}, [buildEntity]);


	const handleMonthChange = useCallback((date: Date) => {
		throttle(() => { setMonth(date) }, 300)();
	}, []);

	return (<>
		<Box py={5}>

			<FirestoreCollection
				path={EMPLOYEE_ENTRIES_COLLECTION_PATH}
				where={{
					field: "employeeId",
					operator: "==",
					value: user.uid,
				}}
				orderBy={[orderBy]}
				startAt={startAt}
				endAt={endAt}
			>
				{({ ids, isLoading, path, value }) => {
					return (<>

						<Box minWidth="500px" maxWidth="1000px" height="812px" mx="auto" position="relative">

							{isLoading && <Loader />}

							{!isLoading && <Timesheet
								month={month}
								entries={value as TimesheetEntry[] || []}
								onMonthChange={handleMonthChange}
								onRowSelect={handleRowClick}
							/>}

						</Box>

					</>);
				}}
			</FirestoreCollection>

			{selectedRow && <FirestoreMutation
				type="set"
				path={`${EMPLOYEE_ENTRIES_COLLECTION_PATH}/${selectedRow.id}`}
				children={({ runMutation }) => (<>

					<TimeEntryDialog
						show={true}
						entry={selectedRow as TimesheetEntry}
						onSubmit={async (values) => {
							try {
								await runMutation(values);
								setSelectedRow(undefined);
								notificateDefault("Registro salvo com sucesso.");
							} catch (error) {
								console.error("save:error", error);
								notificateError("Ocorreu um erro ao salvar o registro.");
							}
						}}
						onClose={() => {
							setSelectedRow(undefined);
						}}
					/>

				</>)}
			/>}

			<Fab
				children={<AddIcon />}
				aria-label="Adicionar entrada"
				onClick={handleAddEntry}
				color="primary"
				style={{
					position: "fixed",
					bottom: 40,
					right: 40,
				}}
			/>

		</Box>

	</>);
}

export default withUser(Dashboard);
