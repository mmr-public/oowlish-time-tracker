

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import ptBrDateFnsLocale from "date-fns/locale/pt-BR";
import DateFnsAdapter from "@date-io/date-fns";
import React from "react";
import { Container, createMuiTheme, MuiThemeProvider } from "@material-ui/core";
import { ptBR } from '@material-ui/data-grid';
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { FirebaseAuthProvider } from "@react-firebase/auth";
import { FirestoreProvider } from "@react-firebase/firestore";
import { SnackbarProvider } from 'notistack';
import { authConfig } from "./constants";
import PageHeader from "#/components/PageHeader";
import Routes from "./routes";


const theme = createMuiTheme({
	typography: {
		fontSize: 14
	},
}, ptBR);

function App() {
	return (
		<MuiThemeProvider theme={theme}>

			<SnackbarProvider>

				<MuiPickersUtilsProvider utils={DateFnsAdapter} locale={ptBrDateFnsLocale}>

					<FirebaseAuthProvider firebase={firebase} {...authConfig}>

						<FirestoreProvider {...authConfig} firebase={firebase}>

							<PageHeader />

							<Container component="main">

								<Routes />

							</Container>

						</FirestoreProvider>

					</FirebaseAuthProvider>

				</MuiPickersUtilsProvider>

			</SnackbarProvider>

		</MuiThemeProvider>
	);
}

export default App;
