import Loader from "#/components/Loader";
import ProtectedRoute from "#/components/ProtectedRoute";
import { PAGES } from "#/constants";
import Login from "#/containers/auth/Login";
import Dashboard from "#/containers/dashboard";
import { FirebaseAuthConsumer } from "@react-firebase/auth";
import React, { FunctionComponent, Suspense } from "react";
import {
	BrowserRouter,
	Redirect,
	Route,
	Switch
} from "react-router-dom";


const Routes: FunctionComponent = () => {
	const LazyNotFound = () => <>Not Found</>;

	return (<FirebaseAuthConsumer>
		{({ isSignedIn, providerId }) => {

			if (providerId === null) return (<Loader />)

			return (<BrowserRouter>
				<Suspense fallback={<>Fallback screen</>}>
					<Switch>
						<Route
							exact
							path={PAGES.Root()}
							render={() => <Redirect to={isSignedIn ? PAGES.Dashboard() : PAGES.Login()} />}
						/>
						<Route
							exact
							path={PAGES.Login()}
							render={() => isSignedIn ? <Redirect to={PAGES.Dashboard()} /> : <Login />}
						/>
						<ProtectedRoute
							exact
							path={PAGES.Dashboard()}
							component={Dashboard}
						/>
						<Route path="*" component={LazyNotFound} />
					</Switch>
				</Suspense>
			</BrowserRouter>)
		}}
	</FirebaseAuthConsumer>);
};

export default Routes;