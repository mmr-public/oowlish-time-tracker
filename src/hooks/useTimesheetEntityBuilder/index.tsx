

import { TimesheetEntry } from "#/containers/dashboard/components/TimeEntryDialog";
import { useCallback } from "react";


export type BuilderFunction = (values: TimesheetEntry) => TimesheetEntry

export type Return = [BuilderFunction];

export const generateDocumentKey = (userId: string, date: string) => {
	return window.btoa(`${userId}-${date}`);
};

export function useTimesheetEntityBuilder(userId: string): Return {

	const buildEntity: BuilderFunction = useCallback((values: TimesheetEntry) => ({
		...values,
		id: generateDocumentKey(userId, values.date),
		employeeId: userId,
		_createdAt: values._createdAt || new Date().toISOString(),
		_modifiedAt: values._createdAt ? new Date().toISOString() : null,
	}), [userId]);

	return [buildEntity];
};
