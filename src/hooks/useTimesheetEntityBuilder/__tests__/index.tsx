import { TimesheetEntry } from '#/containers/dashboard/components/TimeEntryDialog';
import { render } from '@testing-library/react';
import React, { FunctionComponent } from 'react';
import { useTimesheetEntityBuilder } from '..';

type Props = {
	userId: string;
	values: TimesheetEntry;
}

const Component: FunctionComponent<Props> = ({ userId, values }) => {
	const [entityBuilder] = useTimesheetEntityBuilder(userId);
	const entity = entityBuilder(values);

	return (<>
		<input
			id={entity.id}
			data-testid={entity.date}
			data-date={entity.date}
			data-employee={entity.employeeId}
			data-start={entity.startTime}
			data-end={entity.endTime}
			data-break={entity.breakTime}
			data-created={entity._createdAt}
			data-modified={entity._modifiedAt}
			readOnly
		/>
	</>)
}

test("should receive a partial entry and complete with required fields for save", () => {

	const userId = "abc";
	const values = {
		date: "2020-01-01",
		startTime: "05:00",
		endTime: "18:00",
		breakTime: "01:00",
	};

	const { getByTestId } = render(<Component userId={userId} values={values} />);

	const elem = getByTestId("2020-01-01");
	const today = new Date().toISOString().slice(0, 10);

	expect(elem.id).toBeTruthy();
	expect(elem.getAttribute("data-employee")).toBe(userId);
	expect(elem.getAttribute("data-date")).toBe("2020-01-01");
	expect(elem.getAttribute("data-start")).toBe("05:00");
	expect(elem.getAttribute("data-end")).toBe("18:00");
	expect(elem.getAttribute("data-break")).toBe("01:00");
	expect(elem.getAttribute("data-created")).toBeTruthy()
	expect(elem.getAttribute("data-created")!.startsWith(today)).toBeTruthy();
	expect(elem.getAttribute("data-modified")).toBeNull();

});

test("should receive a already created entry and update modifiedAt field", () => {

	const userId = "abc";
	const values = {
		date: "2020-01-01",
		startTime: "05:00",
		endTime: "18:00",
		breakTime: "01:00",
		_createdAt: new Date().toISOString(),
	};

	const { getByTestId } = render(<Component userId={userId} values={values} />);

	const elem = getByTestId("2020-01-01");
	const today = new Date().toISOString().slice(0, 10);

	expect(elem.getAttribute("data-created")).toBeTruthy()
	expect(elem.getAttribute("data-created")!.startsWith(today)).toBeTruthy();
	expect(elem.getAttribute("data-modified")!.startsWith(today)).toBeTruthy();
	
});


test("should generate id from user and date", () => {

	const [userId, date]= ["userId", "2020-01-01"];
	const values = {
		date,
		startTime: "05:00",
		endTime: "18:00",
		breakTime: "01:00",
	};

	const { getByTestId } = render(<Component userId={userId} values={values} />);
	const elem = getByTestId(date);
	const expectedId = window.btoa(`${userId}-${date}`);
	
	expect(elem.id).toBe(expectedId);
	expect(elem.getAttribute("data-date")).toBe(date);
	expect(elem.getAttribute("data-employee")).toBe(userId);
	
});

