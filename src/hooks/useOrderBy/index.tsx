import { useEffect, useState } from "react";

export type OrderBy = {
	field: string;
	type?: "asc" | "desc";
};

type Return = [OrderBy];

export function useOrderBy(fieldName: string, order: "asc" | "desc" = "asc"): Return {
	const [state, setState] = useState<OrderBy>({
		field: fieldName,
		type: order,
	});

	useEffect(() => {
		setState({
			field: fieldName,
			type: order,
		});
	}, [fieldName, order]);

	return [state];
};
