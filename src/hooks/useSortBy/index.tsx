import { GridSortDirection, GridSortItem } from "@material-ui/data-grid";
import { useEffect, useState } from "react";


type Return = [GridSortItem];

export function useSortBy(fieldName: string, order: GridSortDirection = "asc"): Return {
	const [state, setState] = useState<GridSortItem>({
		field: fieldName,
		sort: order,
	});

	useEffect(() => {
		setState({
			field: fieldName,
			sort: order,
		});
	}, [fieldName, order]);

	return [state];
};
