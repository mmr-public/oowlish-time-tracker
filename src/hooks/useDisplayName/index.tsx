import { useAuthentication } from "#/hooks/useAuthentication";
import { defaultTo } from "lodash";
import { useEffect, useState } from "react";

type Return = [string];

const extractUsernameFromEmail = (email?: string | null) => {
	return defaultTo(email?.split("@").shift(), "");
};


export function useDisplayName(): Return {
	const [, user] = useAuthentication();
	const [displayName, setDisplayName] = useState("");

	useEffect(() => {
		if (!user) {
			setDisplayName("");
			return
		}
		const username = user?.displayName
			? user.displayName
			: extractUsernameFromEmail(user.email);

		setDisplayName(username);
	}, [user])

	return [displayName];
}
