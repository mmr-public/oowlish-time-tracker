import { TimesheetEntry } from "#/containers/dashboard/components/TimeEntryDialog";
import { render } from '@testing-library/react';
import dayjs from "dayjs";
import React, { Fragment, FunctionComponent } from 'react';
import { useFilledTimesheet } from "..";

type Props = { entries: TimesheetEntry[], date: Date }

const Component: FunctionComponent<Props> = ({ entries, date }) => {
	const [timesheet] = useFilledTimesheet(entries, date);

	return (<>
		<div data-testid="length">{timesheet.length}</div>

		<div data-testid="items">
			{timesheet
				.map((item, i) => (<Fragment key={`${item.id}`}>
					<input

						id={item.id}
						value={item.date}
						data-testid={`item-${item.date}`}
						data-date={item.date}
						data-starttime={item.startTime}
						data-endtime={item.endTime}
						data-breaktime={item.breakTime}
						data-empyloyee={item.employeeId}
						readOnly
					/>
				</Fragment>))}
		</div>
	</>)
}

test("should render a list with empty values and all days of month", () => {
	const date = dayjs("2020-01-01").toDate();
	const { getByTestId, } = render(<>
		<Component entries={[]} date={date} />
	</>);

	const daysInMonth = dayjs(date).daysInMonth();
	const items = getByTestId("items").children;

	expect(items).toHaveLength(daysInMonth);

	Array
		.from(items)
		.forEach((item, i) => {
			const expectedDate = new Date(2020, 0, i + 1).toISOString().slice(0, 10);
			const elem = getByTestId(`item-${expectedDate}`);

			expect(elem.id).toBe(expectedDate);
			expect(elem.getAttribute("data-date")).toBe(expectedDate);
			expect(elem.getAttribute("data-starttime")).toBeNull();
			expect(elem.getAttribute("data-endtime")).toBeNull();
			expect(elem.getAttribute("data-breaktime")).toBeNull();
		});
});

test("should render a list with merged values and all days of month", () => {
	const entries: TimesheetEntry[] = [
		{
			id: "1",
			date: "2020-01-01",
			startTime: "09:00",
			endTime: "18:00",
			breakTime: "01:00",
			employeeId: "employeeId1"
		},
		{
			id: "2",
			date: "2020-01-15",
			startTime: "10:00",
			endTime: "19:00",
			breakTime: "02:00",
			employeeId: "employeeId2"
		}
	];
	const date = dayjs("2020-01-01").toDate();
	const { getByTestId, } = render(<>
		<Component entries={entries} date={date} />
	</>);

	const daysInMonth = dayjs(date).daysInMonth();
	const items = getByTestId("items").children;
	const filledItems = Array
		.from(items)
		.filter(item => item.id !== "1" && item.id !== "2");

	expect(items).toHaveLength(daysInMonth);
	expect(filledItems).toHaveLength(daysInMonth - 2);

	const entry1 = getByTestId("item-2020-01-01")
	expect(entry1.id).toBe("1");
	expect(entry1.getAttribute("data-date")).toBe("2020-01-01");
	expect(entry1.getAttribute("data-starttime")).toBe("09:00");
	expect(entry1.getAttribute("data-endtime")).toBe("18:00");
	expect(entry1.getAttribute("data-breaktime")).toBe("01:00");
	expect(entry1.getAttribute("data-empyloyee")).toBe("employeeId1");

	const entry2 = getByTestId("item-2020-01-15")
	expect(entry2.id).toBe("2");
	expect(entry2.getAttribute("data-date")).toBe("2020-01-15");
	expect(entry2.getAttribute("data-starttime")).toBe("10:00");
	expect(entry2.getAttribute("data-endtime")).toBe("19:00");
	expect(entry2.getAttribute("data-breaktime")).toBe("02:00");
	expect(entry2.getAttribute("data-empyloyee")).toBe("employeeId2");

	filledItems.forEach((item, i) => {
		const elem = getByTestId(`item-${item.id}`);

		expect(item.id).toHaveLength(10);
		expect(elem.getAttribute("data-date")).toBe(item.id);
		expect(elem.getAttribute("data-starttime")).toBeNull();
		expect(elem.getAttribute("data-endtime")).toBeNull();
		expect(elem.getAttribute("data-breaktime")).toBeNull();
	});
});


