import { TimesheetEntry } from "#/containers/dashboard/components/TimeEntryDialog";
import { buildISOMonthDates, formatDate, ISO_DATE_FORMAT } from "#/utils/date.utils";
import { unionBy } from "lodash";
import { useEffect, useState } from "react";



const fillWithRemainingDays = (values: TimesheetEntry[], activeMonth: Date) => {
	const today = formatDate(new Date(), ISO_DATE_FORMAT);
	const remainingValues = buildISOMonthDates(activeMonth)
		.filter(item => item <= today)
		.map<TimesheetEntry>((item, i) => ({
			id: item,
			employeeId: "",
			date: item,
			_createdAt: "",
		}));
	return unionBy(values, remainingValues, (item: TimesheetEntry) => item.date)
};

type Return = [TimesheetEntry[]];

export function useFilledTimesheet(entries: TimesheetEntry[], timesheetMonth: Date): Return {
	const [state, setState] = useState<TimesheetEntry[]>(fillWithRemainingDays(entries, timesheetMonth));

	useEffect(() => {
		setState(fillWithRemainingDays(entries, timesheetMonth));
	}, [entries, timesheetMonth]);

	return [state];
};
