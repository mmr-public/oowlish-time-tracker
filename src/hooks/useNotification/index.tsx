
import { SnackbarMessage, useSnackbar } from 'notistack';

type NotiftyFunction = (message: SnackbarMessage) => void;

type Return = [{
	notificateSuccess: NotiftyFunction;
	notificateError: NotiftyFunction;
	notificateInfo: NotiftyFunction;
	notificateWarning: NotiftyFunction;
	notificateDefault: NotiftyFunction;
}];

export function useNotification(): Return {
	const { enqueueSnackbar } = useSnackbar();

	const notificateSuccess = (message: SnackbarMessage) => {
		enqueueSnackbar(message, { variant: "success" });
	}
	const notificateError = (message: SnackbarMessage) => {
		enqueueSnackbar(message, { variant: "error" });
	}
	const notificateInfo = (message: SnackbarMessage) => {
		enqueueSnackbar(message, { variant: "info" });
	}
	const notificateWarning = (message: SnackbarMessage) => {
		enqueueSnackbar(message, { variant: "warning" });
	}
	const notificateDefault= (message: SnackbarMessage) => {
		enqueueSnackbar(message, { variant: "default" });
	}

	return [{
		notificateSuccess,
		notificateError,
		notificateInfo,
		notificateWarning,
		notificateDefault,
	}];
};
