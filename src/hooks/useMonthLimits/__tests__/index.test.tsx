import { render } from '@testing-library/react';
import React, { FunctionComponent } from 'react';
import { useMonthLimits } from '..';

type Props = { date: Date }

const Component: FunctionComponent<Props> = ({ date }) => {
	const [limits] = useMonthLimits(date);

	return (<>
		<input readOnly data-testid="startAt" value={limits.startAt} />
		<input readOnly data-testid="endAt" value={limits.endAt} />
	</>)
}

test("return render month start and end date in iso format", () => {

	const { rerender, getByTestId } = render(<Component date={new Date("2020-01-15")} />);

	expect(getByTestId("startAt")).toHaveValue("2020-01-01");
	expect(getByTestId("endAt")).toHaveValue("2020-01-31");


	rerender(<Component date={new Date("2020-02-15")} />);
	expect(getByTestId("startAt")).toHaveValue("2020-02-01");
	expect(getByTestId("endAt")).toHaveValue("2020-02-29");


	rerender(<Component date={new Date("2021-02-15")} />);
	expect(getByTestId("startAt")).toHaveValue("2021-02-01");
	expect(getByTestId("endAt")).toHaveValue("2021-02-28");


	rerender(<Component date={new Date("2021-11-15")} />);
	expect(getByTestId("startAt")).toHaveValue("2021-11-01");
	expect(getByTestId("endAt")).toHaveValue("2021-11-30");
	

	rerender(<Component date={new Date("")} />);
	expect(getByTestId("startAt")).toHaveValue("Invalid Date");
	expect(getByTestId("endAt")).toHaveValue("Invalid Date");
});

