import { endOfMonth, formatDate, startOfMonth } from "#/utils/date.utils";
import { useEffect, useState } from "react";

export type MonthLimits = {
	startAt: string;
	endAt: string;
};

type Return = [MonthLimits];

export function useMonthLimits(date: Date): Return {
	const [state, setState] = useState<MonthLimits>({
		startAt: formatDate(startOfMonth(date)),
		endAt: formatDate(endOfMonth(date)),
	});

	useEffect(() => {
		setState({
			startAt: formatDate(startOfMonth(date)),
			endAt: formatDate(endOfMonth(date)),
		});
	}, [date]);

	return [state];
};
