import { WORK_DAY_HOURS } from "#/constants";
import { TimesheetEntry } from "#/containers/dashboard/components/TimeEntryDialog";
import { buildMonthDates, Duration, isWeekend } from "#/utils/date.utils";
import { calculateWorkedHours } from "#/utils/timesheet.utils";
import dayjs from "dayjs";
import { useEffect, useState } from "react";

type Statistics = {
	expectedHours: Duration;
	workedHours: Duration;
	balance: Duration;
}

type Return = [Statistics];

const calculateMonthExpectedHours = (date: Date) => {
	const workingDayDuration = dayjs.duration(WORK_DAY_HOURS, "hour").asMilliseconds();
	const workingDays = buildMonthDates(date)
		.filter(item => !isWeekend(item))
		.length;

	return dayjs.duration(workingDayDuration * workingDays);
};

const calculateMonthWorkedHours = (entries: TimesheetEntry[]) => {
	const workedHours = entries
		.filter(item => item.startTime && item.endTime)
		.map(item => calculateWorkedHours(item.startTime!, item.endTime!, item.breakTime!))
		.reduce((accumulator, item) => {
			return accumulator.add(item.asMilliseconds());
		}, dayjs.duration(0));


	return workedHours;
};

const calculateBalance = (expectedHours: Duration, workedHours: Duration) => {
	const balance = workedHours.subtract(expectedHours.asMilliseconds());
	
	return balance;
};

export function useMonthStatistics(date: Date, entries: TimesheetEntry[]): Return {
	const [expectedHours, setExpectedHours] = useState(calculateMonthExpectedHours(date));
	const [workedHours, setWorkedHours] = useState(calculateMonthWorkedHours(entries));
	const [balance, setBalance] = useState(dayjs.duration(0));

	useEffect(() => {
		setExpectedHours(calculateMonthExpectedHours(date));
		setWorkedHours(calculateMonthWorkedHours(entries));
	}, [date, entries]);

	useEffect(() => {
		setBalance(calculateBalance(expectedHours, workedHours));
	}, [expectedHours, workedHours]);


	return [{
		expectedHours,
		workedHours,
		balance,
	}];
};
