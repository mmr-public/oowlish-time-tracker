import { TimesheetEntry } from "#/containers/dashboard/components/TimeEntryDialog";
import { formatDuration } from "#/utils/date.utils";
import { render } from '@testing-library/react';
import dayjs from "dayjs";
import "dayjs/locale/pt-br";
import customParseFormat from "dayjs/plugin/customParseFormat";
import duration from "dayjs/plugin/duration";
import isSameOrAfter from "dayjs/plugin/isSameOrAfter";
import relativeTime from "dayjs/plugin/relativeTime";
import utc from "dayjs/plugin/utc";
import React, { FunctionComponent } from 'react';
import { useMonthStatistics } from "..";


function setup() {
	// dayjs
	dayjs.extend(isSameOrAfter);
	dayjs.extend(relativeTime);
	dayjs.extend(duration);
	dayjs.extend(customParseFormat);
	dayjs.extend(utc)
	dayjs.locale("pt-br");
}

type Props = { entries: TimesheetEntry[], date: Date }

const entries: TimesheetEntry[] = [
	{
		id: "1",
		date: "2020-01-01",
		startTime: "09:00",
		endTime: "18:00",
		breakTime: "01:00",
		employeeId: "employeeId1"
	},
	{
		id: "2",
		date: "2020-01-15",
		startTime: "10:00",
		endTime: "19:00",
		breakTime: "02:00",
		employeeId: "employeeId2"
	}
];

const Component: FunctionComponent<Props> = ({ entries, date }) => {
	const [statistics] = useMonthStatistics(date, entries);

	return (<>
		<input
			data-testid="workedHours"
			readOnly
			value={formatDuration(statistics.workedHours.asMilliseconds())}
		/>
		<input
			data-testid="expectedHours"
			readOnly
			value={formatDuration(statistics.expectedHours.asMilliseconds())}
		/>
		<input
			data-testid="balance"
			readOnly
			value={formatDuration(statistics.balance.asMilliseconds())}
		/>
	</>)
}

test("should calculate worked hours in a month", () => {

	setup();

	const date = dayjs("2020-01-01").toDate();
	const { rerender, getByTestId, } = render(<Component date={date} entries={[]} />);

	const elem = getByTestId("workedHours");
	expect(elem).toHaveValue("00:00");


	rerender(<Component date={date} entries={[...entries]} />);
	expect(elem).toHaveValue("15:00");

	const entry = {
		id: "3",
		date: "2020-01-16",
		startTime: "08:00",
		endTime: "18:00",
		breakTime: "01:00",
		employeeId: "employeeId3",
	};
	rerender(<Component date={date} entries={[...entries, entry]} />);
	expect(elem).toHaveValue("24:00");

});


test("should expected hours to work in a month based on week days", () => {
	
	setup();

	const jan = dayjs("2020-01-01").toDate();
	const { rerender, getByTestId, } = render(<Component date={jan} entries={[]} />);

	const elem = getByTestId("expectedHours");
	expect(elem).toHaveValue("184:00");
	
	const feb = dayjs(jan).add(1, "month").toDate();
	rerender(<Component date={feb} entries={[]} />);
	expect(elem).toHaveValue("160:00");

});

test("should calculate balance between worked hours and expected hours", () => {

	setup();

	const date = dayjs("2020-01-01").toDate();
	const { rerender, getByTestId, } = render(<Component date={date} entries={[]} />);

	const workedHours = getByTestId("workedHours");
	const expectedHours = getByTestId("expectedHours");
	const balance = getByTestId("balance");
	
	expect(workedHours).toHaveValue("00:00");
	expect(expectedHours).toHaveValue("184:00");
	expect(balance).toHaveValue("-184:00");


	rerender(<Component date={date} entries={[...entries]} />);	
	expect(workedHours).toHaveValue("15:00");
	expect(expectedHours).toHaveValue("184:00");
	expect(balance).toHaveValue("-169:00");

	const entry = {
		id: "3",
		date: "2020-01-16",
		startTime: "08:00",
		endTime: "18:00",
		breakTime: "01:00",
		employeeId: "employeeId3",
	};
	rerender(<Component date={date} entries={[...entries, entry]} />);
	expect(workedHours).toHaveValue("24:00");
	expect(expectedHours).toHaveValue("184:00");
	expect(balance).toHaveValue("-160:00");

});