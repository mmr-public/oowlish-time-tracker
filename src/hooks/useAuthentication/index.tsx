import firebase from "firebase/app";

const firebaseAuth = firebase.auth;

type Return = [
	{
		createAccount: (email: string, password: string) => Promise<firebase.auth.UserCredential>
		loginWithEmail: (email: string, password: string) => Promise<firebase.auth.UserCredential>
		loginWithGoogle: () => Promise<firebase.auth.UserCredential>
		logout: () => Promise<void>
		fetchSignInMethodsForEmail: (email: string) => Promise<string[]>
	},
	firebase.User | null,
]

export function useAuthentication(): Return {

	const loginWithGoogle = () => {
		const googleAuthProvider = new firebaseAuth.GoogleAuthProvider();
		return firebaseAuth().signInWithPopup(googleAuthProvider);
	};

	const loginWithEmail = (email: string, password: string) => {
		return firebaseAuth()
			.signInWithEmailAndPassword(email, password);
	};

	const createAccount = (email: string, password: string) => {
		return firebaseAuth()
			.createUserWithEmailAndPassword(email, password);
	};

	const fetchSignInMethodsForEmail = (email: string) => {
		return firebaseAuth()
			.fetchSignInMethodsForEmail(email);
	};

	const logout = () => firebaseAuth().signOut();;

	return [
		{
			createAccount,
			loginWithEmail,
			loginWithGoogle,
			logout,
			fetchSignInMethodsForEmail,
		},
		firebaseAuth().currentUser,
	];
}
