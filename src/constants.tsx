export const authConfig = {
	appId: process.env.REACT_APP_FIREBASE_API_KEY,
	apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
	authDomain: "oowlish-time-tracker.firebaseapp.com",
	databaseURL: "https://oowlish-time-tracker-default-rtdb.firebaseio.com",
	projectId: "oowlish-time-tracker",
	storageBucket: "oowlish-time-tracker.appspot.com",
	messagingSenderId: "856461928910",
	measurementId: "G-Y5X33JGZR0"
};

export const PAGES = Object.freeze({
	Root: () => "/",
	Login: () => "/login",
	Dashboard: () => "/dashboard",
});

export const EMPLOYEE_ENTRIES_COLLECTION_PATH = "employee_entries";

export const WORK_DAY_HOURS = 8;