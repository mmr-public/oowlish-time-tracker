import { FirebaseAuthConsumer } from "@react-firebase/auth";
import { AuthEmission } from "@react-firebase/auth/dist/types";
import firebase from "firebase/app";
import React from "react";

export type User = firebase.User;


export type Props = {
	user: User;
	authEmission: AuthEmission;
};

function withUser<P extends {}>(Component: React.ComponentType<P>) {

	const WithUser: React.FunctionComponent<Props & P> = (props) => {
		return (<FirebaseAuthConsumer>
			{(authEmission) => {
				return (<>
					{authEmission.user && <Component {...props} user={authEmission.user} authEmission={authEmission} />}
				</>);
			}}
		</FirebaseAuthConsumer>);
	};

	return WithUser;
}

export default withUser;