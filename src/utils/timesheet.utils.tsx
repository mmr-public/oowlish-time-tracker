import { WORK_DAY_HOURS } from "#/constants";
import { durationBetween, durationFromTime, formatDate, formatDuration, isWeekend } from "#/utils/date.utils";
import { GridCellClassFn, GridCellParams, ValueGetterParams } from "@material-ui/data-grid";
import clsx from "clsx";
import dayjs from "dayjs";
import { isNumber } from "lodash";


export const calculateWorkedHours = (startTime?: string, endTime?: string, breakTime?: string) => {
	if (!startTime || !endTime)
		return dayjs.duration(0);

	const breakDuration = breakTime
		? durationFromTime(breakTime).asMilliseconds()
		: 0;
	return durationBetween(startTime, endTime)
		.subtract(breakDuration, "millisecond");
};

export const calculateRemainingHours = (startTime?: string, endTime?: string, breakTime?: string) => {
	if (!startTime || !endTime)
		return dayjs.duration(0).subtract(WORK_DAY_HOURS, "hour");
	return calculateWorkedHours(startTime, endTime, breakTime)
		.subtract(WORK_DAY_HOURS, "hour");
};

export const workingHoursGetter = ({ row }: ValueGetterParams) => {
	try {
		return calculateWorkedHours(row.startTime, row.endTime, row.breakTime).asMilliseconds();
	} catch (error) {
		console.warn("Error when calculating worked hours", row, error);
		return "–";
	}
};

export const remainingHoursGetter = ({ row }: ValueGetterParams) => {
	try {
		if (isWeekend(row.date))
			return calculateWorkedHours(row.startTime, row.endTime, row.breakTime).asMilliseconds();

		return calculateRemainingHours(row.startTime, row.endTime, row.breakTime).asMilliseconds();
	} catch (error) {
		console.warn("Error when calculating remaining hours", row, error);
		return "–";
	}
};

export const workingHoursFormatter = ({ row }: GridCellParams) => {
	try {
		return calculateWorkedHours(row.startTime, row.endTime, row.breakTime).asMilliseconds();
	} catch (error) {
		console.warn("Error when calculating worked hours", row, error);
		return "–";
	}
};

export const durationFormatter = ({ value, row, field, getValue }: GridCellParams) => {
	if (field === "remainingHours" && isWeekend(row.date) && getValue(field) as number <= 0)
		return " ";
	return isNumber(value) ? formatDuration(value) : value
};

export const dateFieldFormatter = ({ value }: GridCellParams) => `${formatDate(value as string, "DD/MM/YYYY - ddd")}`


export const resolveCellCssClass: GridCellClassFn = ({ getValue, field, value }) => {
	const _isWeekend = isWeekend(getValue("date")?.toString());
	const remainingHours = field === "remainingHours" ? getValue(field) as number : 0;


	return clsx("cell", {
		"--primary": !_isWeekend,
		"--secondary": _isWeekend,
		"--success": remainingHours > 0,
		"--danger": remainingHours < 0,

	});
};
