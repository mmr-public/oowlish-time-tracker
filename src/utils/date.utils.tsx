
import dayjs from "dayjs";
import { padStart, toNumber } from "lodash";

export type ParsableDate = dayjs.ConfigType;

export type Duration = plugin.Duration;

export const LOCALE_ID = "pt-BR"
export const ISO_DATE_FORMAT = "YYYY-MM-DD";
export const LOCALE_DATE_FORMAT = "DD/MM/YYYY";
export const DATE_FNS_LOCALE_DATE_FORMAT = "dd/MM/yyyy";
export const LOCAL_TIME_FORMAT = "HH:mm";


export const formatDate = (date: ParsableDate, dateFormat: string = "YYYY-MM-DD") => {
	return parseDate(date).format(dateFormat);
}

export const parseDate = (date: ParsableDate, format?: string) => {
	// if (!date)
	// 	throw new Error("'date' can not be null.");

	return dayjs(date, format, LOCALE_ID);
}

export const isValidDate = (date?: ParsableDate | null) =>
	date ? dayjs(date).isValid() : false;

export const isValidTime = (time?: string) => {
	if (!time) return false;
	return !time.includes("_") && parseDate(time, LOCAL_TIME_FORMAT).isValid();
}

export const buildMonthDates = (date: Date) => {
	const startOfMonth = parseDate(date).startOf("month");
	const daysInMonth = startOfMonth.daysInMonth();
	return Array
		.from({ length: daysInMonth }, (x, i) => startOfMonth.add(i, "day").toDate())
		.sort((a, b) => a.getTime() - b.getTime());
};

export const buildISOMonthDates = (date: Date) => {
	const monthDates = buildMonthDates(date);
	return monthDates.map(item => formatDate(item));
};

export const durationBetween = (dateA: ParsableDate, dateB: ParsableDate) =>
	dayjs.duration(dayjs(dateB, "HH:mm").diff(dayjs(dateA, "HH:mm")));


export const durationFromTime = (time: string) => {
	if (!isValidTime(time))
		throw new Error("Invalid time format");

	const [hours, minutes] = time
		.split(":")
		.map(toNumber);

	return dayjs.duration({
		hours,
		minutes,
	});
};

export const isAfter = (dateA: ParsableDate, dateB: ParsableDate) => {
	return dayjs(dateA).isAfter(dateB)
}

export const startOfMonth = (date: ParsableDate) =>
	dayjs(date).startOf('month').toDate();

export const endOfMonth = (date: ParsableDate) =>
	dayjs(date).endOf('month').toDate();

export const isWeekend = (date?: ParsableDate) => {
	const weekDay = dayjs(date).day();
	return weekDay === 0 || weekDay === 6;
};

export const formatDuration = (duration: number) => {
	const preffix = duration < 0 ? "-" : "";
	const MINUTES_IN_HOUR = 60;
	const totalMinutes = Math.abs(dayjs.duration(duration).asMinutes());
	const totalHours = totalMinutes / MINUTES_IN_HOUR;
	const partialHours = Math.floor(totalHours);
	const partialMinutes = Math.round((totalHours - partialHours) * MINUTES_IN_HOUR);
	const hours = padStart(`${partialHours}`, 2, "0")
	const minutes = padStart(`${partialMinutes}`, 2, "0")

	return `${preffix}${padStart(hours, 2, "0")}:${padStart(minutes, 2, "0")}`;
};