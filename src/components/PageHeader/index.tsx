
import { useAuthentication } from "#/hooks/useAuthentication";
import { useDisplayName } from "#/hooks/useDisplayName";
import { AppBar, Box, IconButton, Menu, MenuItem, Toolbar, Typography } from "@material-ui/core";
import AccountCircle from '@material-ui/icons/AccountCircle';
import React, { FunctionComponent, MouseEvent, useState } from 'react';


const PageHeader: FunctionComponent = () => {
	const [{ logout }, user] = useAuthentication();
	const [displayName] = useDisplayName();
	const [anchoredMenu, setAnchoredMenu] = useState<HTMLElement>();


	const handleProfileClick = (event: MouseEvent<HTMLElement>) => {
		setAnchoredMenu(event.currentTarget);
	};

	const handleCloseMenu = () => {
		setAnchoredMenu(undefined);
	};


	return (

		<AppBar position="static">

			<Toolbar>

				<Box width="100%" display="flex" flexDirection="row" alignItems="center" justifyContent="flex-end">

					{user &&
						<>
							<Typography children={displayName} />

							<IconButton
								children={<AccountCircle />}
								onClick={handleProfileClick}
								aria-label="account of current user"
								aria-controls="menu-appbar"
								aria-haspopup="true"
								color="inherit"
							/>

							<Menu
								anchorEl={anchoredMenu}
								keepMounted
								open={!!anchoredMenu}
								onClose={handleCloseMenu}
							>
								<MenuItem
									children={"Logout"}
									onClick={logout}
								/>
							</Menu>
						</>
					}

				</Box>

			</Toolbar>

		</AppBar>
	);
};

export default PageHeader;