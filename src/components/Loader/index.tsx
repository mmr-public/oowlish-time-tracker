import { Box, CircularProgress } from '@material-ui/core';
import React, { FunctionComponent } from 'react';

const Loader: FunctionComponent = () => {
	return (
		<Box
			display="flex"
			alignItems="center"
			justifyContent="center"
			alignContent="center"
			height="100%"
			children={<CircularProgress />}
		/>
	);
};

export default Loader;