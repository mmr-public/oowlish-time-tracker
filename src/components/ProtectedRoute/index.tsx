
import { PAGES } from "#/constants";
import { FirebaseAuthConsumer } from "@react-firebase/auth";
import React, { FunctionComponent } from "react";
import {
	Redirect,
	Route,
	RouteProps
} from "react-router-dom";

const ProtectedRoute: FunctionComponent<RouteProps> = (props) => {
	return (
		<FirebaseAuthConsumer>
			{({ isSignedIn }) => {
				return (isSignedIn ? <Route {...props} /> : <Redirect to={PAGES.Login()} />);
			}}
		</FirebaseAuthConsumer>
	);

}

export default ProtectedRoute;