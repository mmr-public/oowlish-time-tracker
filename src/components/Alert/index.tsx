import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import React, { FunctionComponent } from 'react';

const Alert: FunctionComponent<AlertProps> = (props: AlertProps) => {
	return (<MuiAlert elevation={6} variant="filled" {...props} />);
};

export default Alert;