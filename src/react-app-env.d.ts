/// <reference types="react-scripts" />
declare namespace NodeJS {
  interface ProcessEnv {
    readonly REACT_APP_FIREBASE_ID: string;
    readonly REACT_APP_FIREBASE_API_KEY: string;
  }
}