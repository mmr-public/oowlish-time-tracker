# oowlish-employee-time-tracker

Aplicação desenvolvida para registrar as horas trabalhadas em um mês.

A aplicação fará o registro das horas do mês e fará o calculo de horas trabalhadas e o saldo de horário considerando 8h de trabalho nos dias uteis. Sem contabilizar feriados.

Para regisrar as horas o usuário deve selecionar o mês de vigência na parte superior da tabela, e clicar no dia que deseja editar o registro, ou no botão de adicionar registro no canto inferior direto da tela.

O login é feito através do Google ou apenas inserindo as credenciais.


### Known bbgs

Pode ocorrer um bug ao trocar o mês de vigência e os registros feitos não serem mostrados, ao clicar em alguma linha ou no botão de adionar os registros voltam a ser exibidos corretamente.


## quick-start

```console
npm install
npm start
```

### unit-test

O projeto utiliza [jest](https://jestjs.io/) para rodar os testes unitários, para rodar todos os testes, utilize:

```console
npm test
```

## branchs

O projeto segue as politicas do gitFlow:

* feature/description (Novos desenvolvimentos)
* bugfix/description (Ajustes, correções de bugs)
* hotfix/description (Correção de bugs em produção)

## deploy

O deploy utilizando o comando

```console
firebase deploy
```

## ambientes

A aplicação está hospedada em:

https://oowlish-time-tracker.firebaseapp.com/login
